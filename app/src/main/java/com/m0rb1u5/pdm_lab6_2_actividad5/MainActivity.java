package com.m0rb1u5.pdm_lab6_2_actividad5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   public void zoomBack(View view) {
      startActivity(new Intent(this, SegundaActivity.class));
      overridePendingTransition(R.anim.zoom_back_in,
            R.anim.zoom_back_out);
   }

   public void fade(View view) {
      startActivity(new Intent(this, SegundaActivity.class));
      overridePendingTransition(R.anim.fade_in,
            R.anim.fade_out);
   }

   public void right(View view) {
      startActivity(new Intent(this, SegundaActivity.class));
      overridePendingTransition(R.anim.right_in,
            R.anim.right_out);
   }

   public void back(View view) {
      super.onBackPressed();
   }
}
